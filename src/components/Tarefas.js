import React from "react";

class Tarefas extends React.Component {
  render() {
    const { tarefas } = this.props;
    return (
      <div>
        <h1>tarefas</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Título</th>
              <th scope="col">Deletar</th>
            </tr>
          </thead>
          <tbody>
            {tarefas.map((item, key) => (
              <tr key={key}>
                <th scope="row">{item.id}</th>
                <td>{item.title}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={(id) => this.props.deletar(item.id)}
                  >
                    Deletar
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Tarefas;
