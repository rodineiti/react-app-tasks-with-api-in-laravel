import React from "react";

function Contador(props) {
  return (
    <div className="row">
      <div className="col">
        <h1>Contador</h1>
        <p>
          <span className="badge badge-info">{props.contador}</span>
        </p>
        <button className="btn btn-success" onClick={props.adicionar}>
          Adicionar
        </button>
        <button className="btn btn-danger" onClick={props.retirar}>
          Retirar
        </button>
      </div>
    </div>
  );
}

export default Contador;
