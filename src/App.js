import React from "react";
import axios from "axios";
import Tarefas from "./components/Tarefas";
import Contador from "./components/Contador";

class App extends React.Component {
  state = {
    title: "",
    tarefas: [],
    contador: 0,
    message: "",
  };

  constructor(props) {
    super(props);
    this.setarValorCampo = this.setarValorCampo.bind(this);
    this.enviarDados = this.enviarDados.bind(this);
    this.adicionar = this.adicionar.bind(this);
    this.retirar = this.retirar.bind(this);
  }

  componentDidMount() {
    this.pegarTarefas();
  }

  adicionar() {
    let contador = this.state.contador;
    contador = contador + 1;
    this.setState({
      contador: contador,
    });
  }

  retirar() {
    let contador = this.state.contador;
    if (contador > 0) {
      contador = contador - 1;
      this.setState({
        contador: contador,
      });
    }
  }

  pegarTarefas = async () => {
    const response = await axios.get("http://localhost:8000/api/v1/tasks");
    this.setState({ tarefas: response.data });
  };

  setarValorCampo(event) {
    this.setState({
      title: event.target.value,
    });
  }

  enviarDados = async (event) => {
    event.preventDefault();
    await axios.post("http://localhost:8000/api/v1/tasks", {
      title: this.state.title,
    });

    this.pegarTarefas();
    this.setState({
      title: "",
      message: "Adicionado com sucesso",
    });
  };

  deletar = async (id) => {
    const response = await axios.delete(
      "http://localhost:8000/api/v1/tasks/" + id
    );
    this.setState({
      message: response.data.message,
    });
    this.pegarTarefas();
  };

  render() {
    return (
      <div className="container text-center">
        <button className="btn btn-info" onClick={() => this.pegarTarefas()}>
          Reload
        </button>
        <hr />
        {this.state.message !== "" && (
          <div className="alert alert-info">{this.state.message}</div>
        )}
        <form method="post" onSubmit={this.enviarDados}>
          <div className="form-group">
            <label htmlFor="title">Título</label>
            <input
              type="text"
              name="title"
              className="form-control"
              value={this.state.title}
              onChange={this.setarValorCampo}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Adicionar
          </button>
        </form>
        <hr />
        <Tarefas tarefas={this.state.tarefas} deletar={this.deletar} />
        <Contador
          contador={this.state.contador}
          adicionar={this.adicionar}
          retirar={this.retirar}
        />
      </div>
    );
  }
}

export default App;
