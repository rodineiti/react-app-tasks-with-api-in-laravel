import React from "react";
import ReactDOM from "react-dom";
import AppHooks from "./AppHooks";

ReactDOM.render(
  <React.StrictMode>
    <AppHooks />
  </React.StrictMode>,
  document.getElementById("root")
);
