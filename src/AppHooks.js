import React, { useState, useEffect } from "react";
import axios from "axios";
import Tarefas from "./components/Tarefas";
import Contador from "./components/Contador";

function AppHooks() {
  const [title, setTitle] = useState("");
  const [tarefas, setTarefas] = useState([]);
  const [contador, setCotador] = useState(0);
  const [message, setMessage] = useState("");

  useEffect(() => {
    pegarTarefas();
  }, []);

  async function pegarTarefas() {
    const response = await axios.get("http://localhost:8000/api/v1/tasks");
    setTarefas(response.data);
  }

  function adicionar() {
    setCotador(contador + 1);
  }

  function retirar() {
    if (contador > 0) {
      setCotador(contador - 1);
    }
  }

  async function enviarDados(event) {
    event.preventDefault();
    await axios.post("http://localhost:8000/api/v1/tasks", {
      title: title,
    });

    pegarTarefas();
    setTitle("");
    setMessage("Adicionado com sucesso");
  }

  async function deletar(id) {
    const response = await axios.delete(
      "http://localhost:8000/api/v1/tasks/" + id
    );
    setMessage(response.data.message);
    pegarTarefas();
  }

  return (
    <div className="container text-center">
      <hr />
      {message !== "" && <div className="alert alert-info">{message}</div>}
      <form method="post" onSubmit={enviarDados}>
        <div className="form-group">
          <label htmlFor="title">Título</label>
          <input
            type="text"
            name="title"
            className="form-control"
            value={title}
            onChange={(event) => setTitle(event.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Adicionar
        </button>
      </form>
      <hr />
      <Tarefas tarefas={tarefas} deletar={deletar} />
      <Contador contador={contador} adicionar={adicionar} retirar={retirar} />
    </div>
  );
}

export default AppHooks;
